inputs:
with inputs.nixpkgs.lib; with inputs.nixpkgs.lib.types; with inputs.nixpkgs.lib.attrsets;
rec {
  mkSubMod = options: config: mkOption {
      type = submodule ({inherit options; inherit config;});
  };
  mkAttrsSubMod = options: config: mkOption {
      type = attrsOf (submodule ({inherit options; inherit config;}));
  };
  mkHosts = {hosts,...}@args: mapAttrs' (n: v: nameValuePair
    (n)
    (let
        hosts-renamed = mapAttrs' (n1: v1: nameValuePair
            (if n1 == n then "self" else n1)
            (v1)
        ) hosts;
        pkgs = import inputs.nixpkgs {
            localSystem = hosts-renamed.self.hw.system;
            config.allowUnfree=true;
            extraOptions = ''
            experimental-features = nix-command flakes
            '';
        };
    in inputs.nixpkgs.lib.nixosSystem {
        system = hosts-renamed.self.hw.system;
        specialArgs = args // inputs;
        modules = [
            ./modules
            inputs.sops-nix.nixosModules.sops
            {
                networking.hostName = n;
                hosts = hosts-renamed;
                nixpkgs.pkgs = pkgs;
            }
        ];
    })) (filterAttrs (n: v: v.isManaged) hosts);
}