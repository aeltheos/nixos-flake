{
  description = "Aeltheos nixos configurations.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    sops-nix.url = "github:Mic92/sops-nix";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, ...}@inputs: 
  let
    custom-utils = import ./custom-utils.nix inputs;

    users = {
      aeltheos = {
        ring = 0;
        hosts.kalecgos.pubKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE7OcFIO9T9kPyknkOJIVUgmlCh2vo0tsBmJX23agQt9";
        hosts.nozdormu.pubKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbsbKJ4//g6cFC+uVflWxMAWqlik82c6fMw5BhTuxBa";
      };
    };
  in with custom-utils; {
    nixosConfigurations = mkHosts {hosts = {
      chromie = {
        ring = 2;
        isManaged = true;
        hw.system = "x86_64-linux";
        hw.isQEMU = true;
        hw.availableKernelModules =  [ "ata_piix" "uhci_hcd" "virtio_pci" "virtio_scsi" "sd_mod" "sr_mod" ];
        hw.fs."/".device = "/dev/sda1";
        hw.boot.type = "legacy";
        hw.boot.devices = ["/dev/sda"];
        sys.networking.hostId = "755b1c60";
        sys.networking.interfaces."ens18".useDHCP = true;
        sys.networking.extraHostNames = ["chromie.adh.crans.org" "185.230.78.193"];
        sys.ssh.pubKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMkB5J0zIB5Acr0d7R87DMuqO6TYqRgxvEwijRHgfsVS";
        sys.users = users;
        services.grafana.enable = true;
        services.prometheus.enable = true;
      };
      kalecgos = {
        ring = 2;
        isManaged = false;
        sys.ssh.pubKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK8aMINjPt5CYl453tDX6wBbakKpSyRa+DJG6bNgDsgQ";
        sys.networking.extraHostNames = [];
        sys.users = users;
      };
      nozdormu = {
        ring = 1;
        isManaged = true;
        sys.networking.hostId = "1144fd1d";
        hw.system = "x86_64-linux";
        hw.availableKernelModules = ["ahci" "ehci_pci" "usb_storage" "usbhid" "sd_mod" "sr_mod"  "kvm-intel" "ixgbe"];
        hw.boot.type = "uefi";
        hw.boot.enableRemoteUnlock =true;
        hw.fs = {
          "/".device = "zroot/nixos";
          "/".fsType = "zfs";
          "/nix".device = "zroot/nixos/nix";
          "/nix".fsType = "zfs";
          "/etc".device = "zroot/nixos/etc";
          "/etc".fsType = "zfs";
          "/var".device = "zdata/var";
          "/var".fsType = "zfs";
          "/root".device = "zdata/root";
          "/root".fsType = "zfs";
          "/boot".device = "/dev/disk/by-uuid/2B46-22D5";
          "/boot".fsType = "vfat";
        };
        sys.ssh.pubKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICmM8R4yIary9Mn5C65qOr5Nso15nZH2f1bI2A1IyvuH";
        sys.networking.extraHostNames = ["nozdormu.adh.crans.org" "185.230.78.5"];
        sys.networking.interfaces."enp1s0f0".useDHCP=true;
        sys.users = users;
        services.grafana.enable = true;
      };
    }; inherit custom-utils;};
  };
}
