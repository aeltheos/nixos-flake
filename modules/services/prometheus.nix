{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config;

    prometheus = mkSubMod {
        enable = mkOption {type=bool; default=false;};
        node.enable = mkOption {type=bool; default=true;};
    } {};
    services = mkSubMod {inherit prometheus;} {};
    hosts = mkAttrsSubMod {inherit services;} {};
in {
    imports = [];

    options = {inherit hosts;};
    
    config = {
        services.prometheus.enable = cfg.hosts.self.services.prometheus.enable;
        services.prometheus.port = 9001;
        services.prometheus.exporters.node = mkIf cfg.hosts.self.services.prometheus.node.enable {
            enable = true;
            enabledCollectors = ["systemd"];
            port = 9002;
        };
        # Only local for now, because i do not want to expose the prometheus sources to the open.
        # Will collect using wireguard private network after.
        services.prometheus.scrapeConfigs = [
            (mkIf cfg.services.prometheus.exporters.node.enable {
                job_name = "node-${cfg.networking.hostName}";
                static_configs = [{
                    targets = ["127.0.0.1:${toString config.services.prometheus.exporters.node.port}"];
                }];
            })
        ];
    };
}