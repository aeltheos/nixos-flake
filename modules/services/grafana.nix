{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config;

    grafana = mkSubMod {
        enable = mkOption {type=bool; default=false;};
    } {};
    services = mkSubMod {inherit grafana;} {};
    hosts = mkAttrsSubMod {inherit services;} {};
in {
    imports = [];

    options = {
        inherit hosts;
    };
    
    config = {
        services.nginx = mkIf cfg.hosts.self.services.grafana.enable {
            enable = true;
            virtualHosts."${cfg.services.grafana.domain}" = {
                enableACME = true;
                forceSSL = true;
                locations."/"= {
                    proxyPass = "http://127.0.0.1:${toString cfg.services.grafana.port}";
                    proxyWebsockets = true;
                };
            };
        };
        services.grafana = mkIf cfg.hosts.self.services.grafana.enable {
            enable = true;
            domain = "grafana.${cfg.hosts.self.sys.networking.name}";
            port = 3000;
            addr = "127.0.0.1";
            security.adminPasswordFile=cfg.sops.secrets."grafana/admin".path;
            # Temporary, only localhost, will fetch all prometheus database over wireguard later.
            provision.enable = true;
            provision.datasources = [(mkIf cfg.services.prometheus.enable {
                name = "prometheus-${cfg.networking.hostName}";
                url = "http://localhost:${toString cfg.services.prometheus.port}";
                type = "prometheus";
                isDefault = true;
            })];
        };
        # Declaring required secrets:
        sops.secrets."grafana/admin" = mkIf cfg.hosts.self.services.grafana.enable {
            sopsFile = ../../secrets/${cfg.networking.hostName}.yaml;
            owner = cfg.users.users.grafana.name;
            group = cfg.users.users.grafana.name;
            restartUnits = [ "grafana.service" ];
        };
    };
}