{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config;

    hosts = mkAttrsSubMod {} {};
in {
    imports = [];

    options = {inherit hosts;};
    
    config = {
        services.nginx.recommendedProxySettings = true;
        services.nginx.recommendedTlsSettings = true;
        networking.firewall = mkIf config.services.nginx.enable {
            allowedTCPPorts = [443 80];
            allowedUDPPorts = [443 80];
        };
        # Accept letsencrypt
        security.acme.acceptTerms = true;
        security.acme.defaults.email = "aeltheos@crans.org";
    };
}