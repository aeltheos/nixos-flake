{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config.hosts.self;
    interfaces = mkAttrsSubMod {
        useDHCP = mkOption {type=bool;};
    } {};
    extraHostNames = mkOption {
        type=listOf str; default=[];
    };
    name = mkOption {type=str;};
    hostId = mkOption {type=str;};
    networking = mkSubMod {inherit hostId;inherit interfaces; inherit extraHostNames; inherit name;} {name = "${config.networking.hostName}.valdrakken.fr";};
    sys = mkSubMod {inherit networking;} {};
    hosts = mkAttrsSubMod {inherit sys;} {};
in {
    imports = [];
    options = {
        inherit hosts;
    };
    config = {
        networking.hostId = cfg.sys.networking.hostId;
        networking.enableIPv6 = true;
        networking.useDHCP = false; # Disable default dhcp. We only want certains 
        networking.interfaces = mapAttrs' (n: v: nameValuePair n v) cfg.sys.networking.interfaces;
    };
}