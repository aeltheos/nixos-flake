{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config;
    ssh = mkSubMod {
        enable = mkOption {type = bool; default=true;};
        pubKey = mkOption {type = str;};
    } {};
    sys = mkSubMod {inherit ssh;} {};
    hosts = mkAttrsSubMod {inherit sys;} {};
in {
    imports = [];
    options = {inherit hosts;};
    config.services.openssh = mkIf cfg.hosts.self.sys.ssh.enable {
        enable = true;
        permitRootLogin = "yes";
        passwordAuthentication = true;
    };
    config.programs.ssh.knownHosts = mapAttrs' (n: v: nameValuePair
            (if n=="self" then "self" else n)
            ({
                extraHostNames = v.sys.networking.extraHostNames;
                publicKey = v.sys.ssh.pubKey;
            })
        ) cfg.hosts;
    # Adding authorized keys for root.
    # 
    config.users.users.root.openssh.authorizedKeys.keys = 
        mapAttrsToList (n: v: "${v.sys.ssh.pubKey} ${n}" )
        (filterAttrs (n: v:
        if cfg.hosts.self.ring == 0 then
            false
        else if cfg.hosts.self.ring == 1 then
            false
        else if cfg.hosts.self.ring == 2 then
            true
        else
            false
        )
    cfg.hosts);
    config.hosts.self.sys.ssh={}; # Sommewhat ugly fix to avoid aving to define sys.ssh.
}