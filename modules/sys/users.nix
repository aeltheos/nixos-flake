{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config;
    users = mkAttrsSubMod {
        ring = mkOption {type = enum [0 1 2]; default = 2;};
        hosts = mkAttrsSubMod {
            pubKey = mkOption {type=str; default="";};
        } {};
    } {}; 
    sys = mkSubMod {inherit users;} {};
    hosts = mkAttrsSubMod {inherit sys;} {};
in {
    imports = [];
    options = {
        inherit hosts;
    };
    config.users = let
        localUsers = filterAttrs (n: v: 
            if cfg.hosts.self.ring == 0 && v.ring == 0 then true
            else if cfg.hosts.self.ring > 0 && v.ring < 2 then true
            else false
        ) cfg.hosts.self.sys.users;
        sudoUsers = filterAttrs (n: v: 
            v.ring == 0 
        ) cfg.hosts.self.sys.users;
    in mkMerge [{
        # Setup same user authorized keys.
        users = mapAttrs (n: v: {
            openssh.authorizedKeys.keys = mapAttrsToList (n: v: "${v.pubKey} ${n}")
                (filterAttrs (n1: v1: 
                cfg.hosts.${if n1==cfg.networking.hostName then "self" else n1}.ring >= cfg.hosts.self.ring)
                localUsers.${n}.hosts);
            isNormalUser=true;
            extraGroups = [(mkIf (v.ring==0) "wheel")];
        }) localUsers;}
        # Setup ring 0 to root authorized keys.
        {
            users.root.openssh.authorizedKeys.keys = foldr (a: b: a++b) [] (mapAttrsToList (n: v:
             mapAttrsToList (n1: v1: v1.pubKey)
            ( filterAttrs (n2: v2: cfg.hosts.${if cfg.networking.hostName==n2 then "self" else n2}.ring >= cfg.hosts.self.ring) v.hosts)) sudoUsers);
        }
    ];
}