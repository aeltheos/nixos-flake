{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config.hosts.self.hw;
    # Qemu guest special args
    qemu-availableKernelModules = [ "virtio_net" "virtio_pci" "virtio_mmio" "virtio_blk" "virtio_scsi" "9p" "9pnet_virtio" ];
    qemu-kernelModules = [ "virtio_balloon" "virtio_console" "virtio_rng" ];
    qemu-postDeviceCommands = "hwclock -s";

    hw = mkSubMod {
        isQEMU = mkOption {type = bool;};
        availableKernelModules = mkOption {type = listOf str;};
        kernelModules = mkOption {type = listOf str;};
    } {
        isQEMU = mkDefault false;
        kernelModules = mkDefault [];
        availableKernelModules = mkDefault [];
    };
    hosts = mkAttrsSubMod {inherit hw;} {};
in {
    options = {
        inherit hosts;
    };
    config = {
        boot.initrd = mkMerge [
            (mkIf cfg.isQEMU {
                availableKernelModules = qemu-availableKernelModules;
                kernelModules = qemu-kernelModules;
                postDeviceCommands = qemu-postDeviceCommands;
            })
            {
                availableKernelModules = cfg.availableKernelModules;
                kernelModules = cfg.kernelModules;
            }
        ];
        hardware.cpu.intel.updateMicrocode = mkDefault config.hardware.enableRedistributableFirmware;
    };
}