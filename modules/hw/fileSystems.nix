{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config.hosts.self;
    fs = mkAttrsSubMod {
        device = mkOption {type=str;};
        isBoot = mkOption {type=bool;};
        fsType = mkOption {type=str;};
        options = mkOption {type=listOf str;};
    } rec {
        fsType = mkDefault "ext4";
        isBoot = mkDefault false;
        options = if (fsType == "zfs") then ["zfsutil"] else ["defaults"];
    };
    hw = mkSubMod {inherit fs;} {};
    hosts = mkAttrsSubMod {inherit hw;} {};
in {
    options = {
        inherit hosts;
    };

    config = {
        fileSystems = mapAttrs (n: v: {
            device = v.device;
            fsType = v.fsType;
            options = v.options;
        }) cfg.hw.fs;
        services.zfs.autoScrub.enable=true;
    };
}