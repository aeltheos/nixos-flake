{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    cfg = config;

    boot = mkSubMod {
        type = mkOption {type = enum ["uefi" "legacy"];};
        devices = mkOption {type = listOf str;};
        enableRemoteUnlock = mkOption {type=bool; default=false;};
    } {};
    hw = mkSubMod {inherit boot;} {};
    hosts = mkAttrsSubMod {inherit hw;} {};
in {
    imports = [./fileSystems.nix];

    options = {
        inherit hosts;
    };
    config.boot.loader = mkMerge [
        {
            grub.enable = true;
            grub.version = 2;
            grub.extraEntries = ''
                menuentry "reboot" { reboot }
                menuentry "shutdown" { halt }
            '';
            grub.copyKernels=true;
        }
        (mkIf (cfg.hosts.self.hw.boot.type == "legacy") {
            grub.devices = cfg.hosts.self.hw.boot.devices;
        })
        (mkIf (cfg.hosts.self.hw.boot.type == "uefi") {
            grub.device = "nodev";
            grub.efiSupport = true;
            grub.efiInstallAsRemovable = true;
            # No support for mirrored boot yet.
        })
    ];
    config.boot.initrd = mkIf cfg.hosts.self.hw.boot.enableRemoteUnlock {
        network.enable = true;
        network.ssh.enable = true;
        network.ssh.port = 2222;
        network.ssh.hostKeys = [cfg.sops.secrets."initrd/hostKey".path];
        network.ssh.authorizedKeys = cfg.users.users.root.openssh.authorizedKeys.keys;
        network.postCommands = ''
            cat <<EOF > /root/.profile
            if pgrep -x "zfs" > /dev/null
            then
                zpool import -a
                zfs load-key -a
                killall zfs
            else
                echo "zfs not running -- maybe the pool is taking some time to load for some unforseen reason."
            fi
            EOF
            '';
    };
    config.sops.secrets."initrd/hostKey" =
     mkIf cfg.hosts.self.hw.boot.enableRemoteUnlock {
         sopsFile = ../../secrets/${cfg.networking.hostName}.yaml;
    };
}