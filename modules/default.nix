{config, pkgs,...}@inputs:
with pkgs.lib; with pkgs.lib.types; with pkgs.lib.attrsets; with inputs.custom-utils;
let
    hw = mkSubMod {
        system = mkOption {type=str;};
    } {};
    # Ring represent how much such a machine can be trusted.
    # 0 is full trust / encrypted laptop.
    # 1 is encrypted with remote unlocking in a "safe" place.
    # 2 is non-encrypted but in a "safe" place.
    # 0 can ssh into 0,1,2; 1 can ssh into 1,2;
    # 1,2 might get better access but with very limited privilege.
    ring = mkOption {type=enum [0 1 2];};
    # This enable adding hosts that are not defined by this flake.
    # But building other hosts base on this unmanaged host.
    # Exemple would be building ssh access based on public key and ring.
    isManaged = mkOption {type=bool;};
    hosts = mkAttrsSubMod {inherit hw; inherit ring; inherit isManaged;} {};
in {
    imports = [./hw/fileSystems.nix ./hw/boot.nix ./hw/kernel.nix ./sys/networking.nix
     ./sys/sops.nix ./sys/openssh.nix ./services/grafana.nix ./services/nginx.nix
      ./services/prometheus.nix ./sys/users.nix];
    options = {
        inherit hosts;
    };
    config = {
        system.stateVersion = "22.11";
        environment.systemPackages = with pkgs; [git vim tmux];
        hosts.self.services.prometheus = {};
    };
}